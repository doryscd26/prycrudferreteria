El proyecto de nombre "PryFerrerteria", permitirá la gestión de la configuración es el registro y actualización detallados de la información del software para trabajar en
contenedores. Un objetivo importante de la gestión de la configuración es asegurar que los cambios realizados del software no afectarán negativamente a cualquiera de los otros softwares.
Para el presente proyecto se utilizará Php7 y MySql8,  Php5.6 y MySql 5.7, mismo que se crearon imagenes para su posterior uso al ejecutar en un docker.
Estructura del proyecto

1.Carpeta src contiene:
1.1 Base de datos "db_articulos"
1.2 El crud de la ferreteria index.php
2.DockerFile
3.docker-compose.yml
